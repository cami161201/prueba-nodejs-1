const { request } = require('express');
const { guardarDB, leerDB } = require('../helpers/guardar');
const Persona = require('../models/persona');
const Personas = require('../models/personas');

const { response } = 'express';

// instanciamos Personas, lo hacemos global para que se añada un nuevo usuario con cada petición POST
const personas = new Personas();

// Método para obtener una persona
// GET //
const personaGet = (req = request, res = response) => {
  const { nombres, apellidos } = req.query;
  const personaDB = leerDB();

  if (personaDB) {
    personas.cargarPersonaFromArray(personaDB);
  }

  res.json({
    msg: 'get API - Controlador',
    // Mostramos aqui la lista de personas
    nombres,
    apellidos,
    personaDB
  })

};

// PUT //
// Método para actulizar una persona
const personaPut = (req, res = response) => {

  const { id } = req.params

  if (id) {
    personas.eliminarPersona(id)
  
    const { nombres, apellidos, ci, direccion, sexo } = req.body;
    // pasamos los parámetros de Persona
    const persona = new Persona(nombres, apellidos, ci , direccion, sexo);
  
    persona.setID(id)
  
    personas.crearPersona(persona);
    guardarDB(personas.listArr);
  }

  res.json({
    msg: 'put API - Controlador'
  })
}

// POST //
// Método para añadir una persona 
const personaPost = (req, res = response) => {
  const { nombres, apellidos, ci, direccion, sexo } = req.body;

  // pasamos los parámetros de Persona
  const persona = new Persona(nombres, apellidos, ci , direccion, sexo);

  let personaDB = leerDB()
  // instanciamos Personas
  // const personas = new Personas();

  // condicion para cargar los datos ya guardados
  if (personaDB) {
    personas.cargarPersonaFromArray(personaDB);
  }
  // añadimos una nueva 'persona' a 'personas'
  personas.crearPersona(persona);
  guardarDB(personas.listArr);
  personaDB = leerDB()

  const listado = leerDB();

  if (listado) {
    personas.cargarPersonaFromArray(listado);
  }

  res.json({
    msg: 'post API - Controlador',
    listado
  })

}

// DELETE //
// Método para eliminar una persona
const personaDelete = (req, res = response) => {

  const { id } = req.params
  
  if (id) {
    personas.eliminarPersona(id);
    console.log("Este es delete:",personas.listArr);
    guardarDB(personas.listArr)
    // console.log(leerDB());
  }

  res.json({
    msg: 'delete API - Controlador',
  })
};


module.exports = {
  personaGet,
  personaPut,
  personaPost,
  personaDelete
}